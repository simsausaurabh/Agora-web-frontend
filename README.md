# ***Front-end for Agora in AngularJs***

## Requirements:
- Angular-Cli
- Node >=v6.*
- npm >=v3.*
## Installation:
- ```npm install -g @angular/cli``` - to install the Angular-Cli.
- ```npm install``` - to install required node modules to run the project.
## Local Deployment:
- ```ng serve``` - run the command and open ```http://localhost:4200``` in your web-browser.